﻿using UnityEngine;
using System.Collections;

public class CloudMove : MonoBehaviour {
    public GameObject cloud;
     
    public float Speed = 1.0f;
    private float x = -7.4f;
    private float y;
    private float z = 0.0f;
    private Transform transformThis;
    // Use this for initialization
    void Start () {
        transformThis = cloud.transform;
        
    }
	
	// Update is called once per frame
	void Update () {
        if (transformThis.position.x > 8.2f) {
            SetPositionAndSpeed();
        }
        transformThis.Translate(Vector3.right * Speed * Time.deltaTime,Space.World);
        
    }

    public void SetPositionAndSpeed() {
        Speed = Random.Range(0.2f, 0.8f);
        y = Random.Range(1.3f, 2.45f);
        transformThis.position = new Vector3(x, y, z);
      }
}
