﻿using UnityEngine;
using System.Collections;

public class PumpTest : MonoBehaviour {
    public GameObject PumpTop;

    public float Speed = 0.0f;
    private float x = -0.61f;
    private float y = 2.35f;
    private float z = 0.0f;
    private Transform transformThis;
    private Vector3 start = new Vector3(-0.61f, 2.35f,0.0f);
    private Vector3 target = new Vector3(-0.61f, 1.35f,0.0f);
    // Use this for initialization
    void Start () {
        transformThis = PumpTop.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transformThis.localPosition;


        if (pos != target)
        {
			transformThis.localPosition = Vector3.MoveTowards(pos, target, Speed);
        }
        else
        {

            target = start;
            if (pos == start)
            {
                target = new Vector3(-0.61f, 1.35f, 0.0f);
            }
        }
    }
	public void setPumpSpeed(float sp){
		Speed += sp;
	}
        
}
