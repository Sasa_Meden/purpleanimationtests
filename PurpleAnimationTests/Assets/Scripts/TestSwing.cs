﻿using UnityEngine;
using System.Collections;

public class TestSwing : MonoBehaviour {
    private Transform transformThis;
    private float rotationSpeed = 1.0f;
    public float maxRotation = 10.0f;
    private int rotationState = 6;
	// Use this for initialization
	void Start () {
        rotationState = 6;
        transformThis = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
        transformThis.rotation = Quaternion.Euler(0.0f, 0.0f, maxRotation * Mathf.Sin(Time.time * rotationSpeed));
        if (transformThis.rotation.z > 150.001f) {
            Debug.Log("150.5");
            maxRotation -= 2.0f;
            rotationState --;
        }
        if (transformThis.rotation.z > 152.001f && rotationState == 5)
        {
            Debug.Log("152.5");
            maxRotation -= 2.0f;
            rotationState--;
        }
        if (transformThis.rotation.z > 15.001f && rotationState == 4)
        {
            Debug.Log("154.5");
            maxRotation -= 2.0f;
            rotationState--;
        }
        if (transformThis.rotation.z > 156.001f && rotationState == 3)
        {
            Debug.Log("156.5");
            maxRotation -= 2.0f;
            rotationState--;
        }
        if (transformThis.rotation.z > 158.001f && rotationState == 2)
        {
            Debug.Log("158.5");
            maxRotation = 0;
            
        }

    }
}
