﻿using UnityEngine;
using System.Collections;

public class TestCollider : MonoBehaviour
{
    private Transform transformThis;
    private GameObject Flower;
    private Transform FlowerCenterPosition;
    public float Speed = 1.0f;
    private Animator ButterflyAnimator;
    public TestCollider script;
    // Use this for initialization
    void Start()
    {
        transformThis = this.transform;
        ButterflyAnimator = this.GetComponentInChildren<Animator>();
        ButterflyAnimator.SetBool("InFlight", false);
    }

    // Update is called once per frame
    void Update()
    {
        transformThis.Translate(Vector3.left * Speed * Time.deltaTime, Space.World);

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ButterflyAnimator.SetBool("InFlight", true);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ButterflyAnimator.SetBool("InFlight", false);
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Balloon")
        {
            // Flower = other.gameObject;
            script = other.gameObject.GetComponent<TestCollider>();
            //FlowerCenterPosition = other.gameObject.transform.Find("TargetPosition").GetComponentInChildren<Transform>();
            Debug.Log("Collider ");
            script.ChangeDirection();
        }
    }
    public void ChangeDirection() {
        Speed *= -1.0f;
    }
}
