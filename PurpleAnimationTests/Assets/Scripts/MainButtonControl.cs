﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class MainButtonControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    public void ClickBalloons() {
        SceneManager.LoadScene("TestScene", LoadSceneMode.Single);
    }

    public void ClickSquares() {
        SceneManager.LoadScene("SceneLoader", LoadSceneMode.Single);
    }
}
