﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SquaresButtonControl : MonoBehaviour {
    public Text btnTxt;
    public GameObject[] spawn;
    public GameObject spawner;
    private Transform spawnPos;
    private static int i = 0;
	// Use this for initialization
	void Start () {
        spawnPos = spawner.transform;
	}
	
	// Update is called once per frame
	public void ClickTooth () {
        
        if (i == 3) {
            i = 0;
        }
        Instantiate(spawn[i], spawnPos.transform.position, spawnPos.transform.rotation);
        i++;
        btnTxt.text = "Clicked " + i;


    }
}
