﻿using UnityEngine;
using System.Collections;

public class ScaleTest : MonoBehaviour {
    public GameObject ballon;
    private Transform transformThis;
    public float scaleSpeed = 0.1f;
    public float scaleDownSpeed = 1.0f;
    public float scaleSize = 1.0f;
    public float scaleDownSize = 0.1f;
    private float ping = 1.0f;
    private bool scaleLimit = false;
    private bool scaleCurrent = true;
    // Use this for initialization
    void Start () {
        transformThis = ballon.transform;
	}
	
	// Update is called once per frame
	void Update () {

        if (transformThis.localScale.x < 1.0f) {

            transformThis.localScale += new Vector3(scaleSize, scaleSize, scaleSize)* scaleSpeed * Time.deltaTime;
        }
        /*if (transformThis.localScale.x >= 1.0f)
        {
            scaleCurrent = false;
             transformThis.localScale -= new Vector3(scaleDownSize, scaleDownSize, scaleDownSize) * scaleDownSpeed * Time.deltaTime;
        }*/

            //  transformThis.localScale += new Vector3(Mathf.PingPong(Time.time, 0.1f), Mathf.PingPong(Time.time, 0.1f), Mathf.PingPong(Time.time, 0.1f)) * scaleSpeed * Time.deltaTime;
            //wait();
        }
    IEnumerator wait() {
       // ping *= -1.0f;

        yield return new WaitForSeconds(0.001f);
    }
}
