﻿using UnityEngine;
using System.Collections;

public class PumpControl : MonoBehaviour {
	public GameObject pumpa;
	private GameObject pumpUp;
	private PumpTest t;
	// Use this for initialization
	void Start () {
		pumpUp = pumpa.transform.Find ("PumpaTop").gameObject;
		t = pumpUp.GetComponent<PumpTest> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			t.setPumpSpeed (0.1f);
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			t.setPumpSpeed (-0.1f);
		}
	}
}
